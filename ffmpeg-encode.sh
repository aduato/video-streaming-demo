input="/home/noroot/Downloads/sintel-2048-surround.mp4"
movie="sintel"

ffmpeg -i $input -c:a copy \
  -vf "scale=-2:272" \
  -vf "drawtext=text='360p':x=10:y=H-th-10:fontfile=/usr/share/fonts/TTF/DroidSans.ttf:fontsize=200:fontcolor=white:shadowcolor=black:shadowx=5:shadowy=5" \
  -c:v libx264 -profile:v baseline -level:v 3.0 \
  -x264-params scenecut=0:open_gop=0:min-keyint=72:keyint=72 \
  -minrate 600k -maxrate 600k -bufsize 600k -b:v 600k \
  -y $movie/encodes/h264_baseline_360p_600.mp4 < /dev/null
  
ffmpeg -i $input -c:a copy \
  -vf "scale=-2:362" \
  -vf "drawtext=text='480p':x=10:y=H-th-10:fontfile=/usr/share/fonts/TTF/DroidSans.ttf:fontsize=200:fontcolor=white:shadowcolor=black:shadowx=5:shadowy=5" \
  -c:v libx264 -profile:v baseline -level:v 3.0 \
  -x264-params scenecut=0:open_gop=0:min-keyint=72:keyint=72 \
  -minrate 1000k -maxrate 1000k -bufsize 1000k -b:v 1000k \
  -y $movie/encodes/h264_main_480p_1000.mp4 < /dev/null

ffmpeg -i $input -c:a copy \
  -vf "scale=-2:544" \
  -vf "drawtext=text='720p':x=10:y=H-th-10:fontfile=/usr/share/fonts/TTF/DroidSans.ttf:fontsize=200:fontcolor=white:shadowcolor=black:shadowx=5:shadowy=5" \
  -c:v libx264 -profile:v baseline -level:v 3.0 \
  -x264-params scenecut=0:open_gop=0:min-keyint=72:keyint=72 \
  -minrate 2000k -maxrate 2000k -bufsize 2000k -b:v 2000k \
  -y $movie/encodes/h264_main_720p_2000.mp4 < /dev/null

ffmpeg -i $input -c:a copy \
  -vf "scale=-2:816" \
  -vf "drawtext=text='1080p':x=10:y=H-th-10:fontfile=/usr/share/fonts/TTF/DroidSans.ttf:fontsize=200:fontcolor=white:shadowcolor=black:shadowx=5:shadowy=5" \
  -c:v libx264 -profile:v baseline -level:v 3.0 \
  -x264-params scenecut=0:open_gop=0:min-keyint=72:keyint=72 \
  -minrate 3000k -maxrate 3000k -bufsize 3000k -b:v 3000k \
  -y $movie/encodes/h264_high_1080p_3000.mp4 < /dev/null


input="/home/noroot/Downloads/bbb_sunflower_1080p_30fps_normal.mp4"
movie="bbb"

ffmpeg -i $input -c:a copy \
  -vf "scale=-2:360" \
  -vf "drawtext=text='360p':x=10:y=H-th-10:fontfile=/usr/share/fonts/TTF/DroidSans.ttf:fontsize=200:fontcolor=white:shadowcolor=black:shadowx=5:shadowy=5" \
  -c:v libx264 -profile:v baseline -level:v 3.0 \
  -x264-params scenecut=0:open_gop=0:min-keyint=72:keyint=72 \
  -minrate 600k -maxrate 600k -bufsize 600k -b:v 600k \
  -y $movie/encodes/h264_baseline_360p_600.mp4 < /dev/null
  
ffmpeg -i $input -c:a copy \
  -vf "scale=-2:480" \
  -vf "drawtext=text='480p':x=10:y=H-th-10:fontfile=/usr/share/fonts/TTF/DroidSans.ttf:fontsize=200:fontcolor=white:shadowcolor=black:shadowx=5:shadowy=5" \
  -c:v libx264 -profile:v baseline -level:v 3.0 \
  -x264-params scenecut=0:open_gop=0:min-keyint=72:keyint=72 \
  -minrate 1000k -maxrate 1000k -bufsize 1000k -b:v 1000k \
  -y $movie/encodes/h264_main_480p_1000.mp4 < /dev/null

ffmpeg -i $input -c:a copy \
  -vf "scale=-2:720" \
  -vf "drawtext=text='720p':x=10:y=H-th-10:fontfile=/usr/share/fonts/TTF/DroidSans.ttf:fontsize=200:fontcolor=white:shadowcolor=black:shadowx=5:shadowy=5" \
  -c:v libx264 -profile:v baseline -level:v 3.0 \
  -x264-params scenecut=0:open_gop=0:min-keyint=72:keyint=72 \
  -minrate 2000k -maxrate 2000k -bufsize 2000k -b:v 2000k \
  -y $movie/encodes/h264_main_720p_2000.mp4 < /dev/null

ffmpeg -i $input -c:a copy \
  -vf "scale=-2:1080" \
  -vf "drawtext=text='1080p':x=10:y=H-th-10:fontfile=/usr/share/fonts/TTF/DroidSans.ttf:fontsize=200:fontcolor=white:shadowcolor=black:shadowx=5:shadowy=5" \
  -c:v libx264 -profile:v baseline -level:v 3.0 \
  -x264-params scenecut=0:open_gop=0:min-keyint=72:keyint=72 \
  -minrate 3000k -maxrate 3000k -bufsize 3000k -b:v 3000k \
  -y $movie/encodes/h264_high_1080p_3000.mp4 < /dev/null


input="/home/noroot/Downloads/03_caminandes_llamigos_1080p.mp4"
movie="caminandes"

ffmpeg -i $input -c:a copy \
  -vf "scale=-2:360" \
  -vf "drawtext=text='360p':x=10:y=H-th-10:fontfile=/usr/share/fonts/TTF/DroidSans.ttf:fontsize=200:fontcolor=white:shadowcolor=black:shadowx=5:shadowy=5" \
  -c:v libx264 -profile:v baseline -level:v 3.0 \
  -x264-params scenecut=0:open_gop=0:min-keyint=72:keyint=72 \
  -minrate 600k -maxrate 600k -bufsize 600k -b:v 600k \
  -y $movie/encodes/h264_baseline_360p_600.mp4 < /dev/null
  
ffmpeg -i $input -c:a copy \
  -vf "scale=-2:480" \
  -vf "drawtext=text='480p':x=10:y=H-th-10:fontfile=/usr/share/fonts/TTF/DroidSans.ttf:fontsize=200:fontcolor=white:shadowcolor=black:shadowx=5:shadowy=5" \
  -c:v libx264 -profile:v baseline -level:v 3.0 \
  -x264-params scenecut=0:open_gop=0:min-keyint=72:keyint=72 \
  -minrate 1000k -maxrate 1000k -bufsize 1000k -b:v 1000k \
  -y $movie/encodes/h264_main_480p_1000.mp4 < /dev/null

ffmpeg -i $input -c:a copy \
  -vf "scale=-2:720" \
  -vf "drawtext=text='720p':x=10:y=H-th-10:fontfile=/usr/share/fonts/TTF/DroidSans.ttf:fontsize=200:fontcolor=white:shadowcolor=black:shadowx=5:shadowy=5" \
  -c:v libx264 -profile:v baseline -level:v 3.0 \
  -x264-params scenecut=0:open_gop=0:min-keyint=72:keyint=72 \
  -minrate 2000k -maxrate 2000k -bufsize 2000k -b:v 2000k \
  -y $movie/encodes/h264_main_720p_2000.mp4 < /dev/null

ffmpeg -i $input -c:a copy \
  -vf "scale=-2:1080" \
  -vf "drawtext=text='1080p':x=10:y=H-th-10:fontfile=/usr/share/fonts/TTF/DroidSans.ttf:fontsize=200:fontcolor=white:shadowcolor=black:shadowx=5:shadowy=5" \
  -c:v libx264 -profile:v baseline -level:v 3.0 \
  -x264-params scenecut=0:open_gop=0:min-keyint=72:keyint=72 \
  -minrate 3000k -maxrate 3000k -bufsize 3000k -b:v 3000k \
  -y $movie/encodes/h264_high_1080p_3000.mp4 < /dev/null