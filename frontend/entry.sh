#!/bin/sh
for i in $(env | grep _LOCATOR); do export $(echo $i | sed "s/\:.*//"); done
exec caddy --conf /etc/Caddyfile --log stdout --agree=$ACME_AGREE