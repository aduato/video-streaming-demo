docker run -v `pwd`:/media -it --net=host --rm google/shaka-packager packager \
  in=/media/encodes/h264_baseline_360p_600.mp4,stream=audio,output=/media/audio.mp4 \
  in=/media/encodes/h264_baseline_360p_600.mp4,stream=video,output=/media/h264_360p.mp4 \
  in=/media/encodes/h264_main_480p_1000.mp4,stream=video,output=/media/h264_480p.mp4 \
  in=/media/encodes/h264_main_720p_2000.mp4,stream=video,output=/media/h264_720p.mp4 \
  in=/media/encodes/h264_high_1080p_3000.mp4,stream=video,output=/media/h264_1080p.mp4 \
  --mpd_output /media/h264.mpd